var mymap = L.map('mapid').setView([52.6, 39.6], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    // maxZoom: 18,
    // id: 'mapbox/streets-v11',
    // tileSize: 512,
    // zoomOffset: -1
}).addTo(mymap);

var xhr = new XMLHttpRequest();

xhr.open('GET', 'https://gis.admlr.lipetsk.ru/api/v1/energoefficiency/list', false);

xhr.send();

if (xhr.status != 200) {
    alert( xhr.status + ': ' + xhr.statusText );
} else {
    // console.log( JSON.parse(xhr.responseText));
}

function onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.popupContent) {
        layer.bindPopup(feature.properties.popupContent);
        // console.log(feature);
    }
}


var markers = new L.MarkerClusterGroup({
    showCoverageOnHover: true,
    maxClusterRadius: 20
});

var geojsonFeature = JSON.parse(xhr.responseText);


var jsonData = L.geoJSON(geojsonFeature, {
    onEachFeature: onEachFeature
});

markers.addLayer(jsonData);
mymap.addLayer(markers);


jsonData.on('data:loaded', function () {});